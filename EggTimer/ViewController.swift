//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    let eggTimes: [String : Float] = ["Soft": 300.0, "Medium": 420.0, "Hard": 720.0]
    
    var timer: Timer = Timer()
    var totalTime: Float = 0.0
    var secondsPassed: Float = 0.0
    var player: AVAudioPlayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressBar.progress = 0.0
        
        let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3")
        player = try! AVAudioPlayer(contentsOf: url!)
    }
    
    @IBAction func hardnessSelected(_ sender: UIButton) {
        
        timer.invalidate()
        progressBar.progress = 0.0
        secondsPassed = 0.0
        
        let hardness = sender.currentTitle!
        titleLabel.text = hardness
        
        totalTime = eggTimes[hardness]!
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }

    @objc func updateTimer() {
        print("Entro al timer: \(secondsPassed)")
        if secondsPassed < totalTime {
            secondsPassed += 1
            
            let percentageProgress = secondsPassed / totalTime
            
            progressBar.progress = percentageProgress
        } else {
            titleLabel.text = "Ready to eat!"
            timer.invalidate()
            
            player.play()
        }
    }
}
